<?php

namespace AhmedAlAhmed\SecuredUrlGenerator;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;

class SecuredFileService implements ISecuredFileService
{
    public function securedStore($file, $path)
    {
        try {
            $savedFilePath = $file->store($path);
            $sig = $this->getSignatureForFile($savedFilePath);

            $finalUrl = $savedFilePath . '?o=' . $sig;

            return $finalUrl;

        } catch (\Exception $exception) {
            throw new \Exception('Invalid Operation');
        }
    }


    protected function getSignatureForFile($filePath)
    {
        return $this->base64url_encode(sha1($filePath . '/\/\\' . sha1($this->base64url_encode($filePath))));
    }

    protected function base64url_encode($data)
    {
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }

    protected function base64url_decode($data)
    {
        return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
    }


    public function getFile($request, $fileName, $prefixStoragePath = 'app/public')
    {
        $path = storage_path($prefixStoragePath) . '/' . $fileName;

        if ($request->query('o', '-1') === '-1') {
            abort(401);
        }

        if (!File::exists($path)) {
            abort(404);
        }

        $sig = $request->query('o');

        $fullPath = $fullPath = url('/storage') . '/' . $prefixStoragePath . '/' . $fileName;
        $fileSig = $this->getSignatureForFile($fullPath);

        if ($fileSig !== $sig) {
            abort(401);
        }


        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    }
}
