<?php


namespace AhmedAlAhmed\SecuredUrlGenerator;


interface ISecuredFileService
{
    public function securedStore($file, $path);

    public function getFile($request, $fileName, $prefixStoragePath = 'app/public');
}
