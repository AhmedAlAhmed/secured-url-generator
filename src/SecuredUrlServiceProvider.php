<?php

namespace AhmedAlAhmed\SecuredUrlGenerator;

use Illuminate\Support\ServiceProvider;

class SecuredUrlServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ISecuredFileService::class, SecuredFileService::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
